# Interplay between needs: and dependencies:

This repo is a companion to my [blog post][blog-post] about the interplay 
between Gitlab's `needs:` and `dependencies:` keywords. The blog post describes
four different configuration versions. You can see them in this repo by checking
out the following branches:

- 1-jobs-blocked-by-expired-artifact
- 2-broken-pipeline
- 3-working-pipeline
- 4-simplified-config



[blog-post]: https://pjsmets.com/gitlab-fixing-the-could-not-retrieve-the-needed-artifacts-error/

